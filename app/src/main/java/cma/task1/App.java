package cma.task1;

import java.io.*;
import java.net.URI;
import java.util.*;

/**
 * Use app.properties to set source(root) directory and output file location.
 */
public class App {

    public void run() throws Exception {

        File srcFile = getFileByPropertyKey("root-directory", false);
        SortedMap<String, File> sortedFiles = getSortedFiles(srcFile, new TreeMap<>());
        File result = mergeAllFiles(sortedFiles);//see output file in location from app.properties
    }

    File getFileByPropertyKey(String key, boolean createFile) throws Exception {

        URI uri = App.class.getResource("/props/app.properties").toURI();
        File props = new File(uri);
        checkIfFileExists(props, false);

        try (FileInputStream fis = new FileInputStream(props)) {
            Properties properties = new Properties();
            properties.load(fis);
            String dirLocation = properties.getProperty(key);
            File f = new File(dirLocation);
            checkIfFileExists(f, createFile);
            return f;
        }
    }

    void checkIfFileExists(File f, boolean createFile) throws Exception {

        if (!f.exists()) {
            if (createFile) {
                f.createNewFile();
                return;
            }
            String err = "file: " + f.getName() + " doesn't exist";
            throw new IllegalArgumentException(err);
        }
    }

    TreeMap<String, File> getSortedFiles(File file, TreeMap<String, File> files) {

        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                if (f.isFile()) {
                    files.put(f.getName(), f);
                } else {
                    getSortedFiles(f, files);
                }
            }
        }
        return files;
    }

    File mergeAllFiles(SortedMap<String, File> files) throws Exception {

        File result = getFileByPropertyKey("output-file", true);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(result))) {
            for (File f : files.values()) {
                writeFile(f, writer);
            }
        }
        return result;
    }

    void writeFile(File f, BufferedWriter writer) throws Exception {

        List<String> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
            String s;
            while ((s = reader.readLine()) != null) {
                list.add(s);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            writer.write(list.get(i) + "\n");
        }
        writer.flush();
    }

    public static void main(String[] args) throws Exception {

        new App().run();
    }
}
