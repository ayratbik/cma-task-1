package cma.task1;

import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.TreeMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class AppTest {

    /**
     * Тест рекурсивного вызова getSortedFiles.
     * <p>
     * Остальные тесты реализую частично, поскольку задача демонстрационная.
     */
    @Test
    public void testGetSortedFiles() throws URISyntaxException {

        URI uri = getClass().getClassLoader().getResource("root-dir").toURI();
        File rootDir = new File(uri);

        // recursively call verifying
        App appSpy = spy(App.class);
        TreeMap<String, File> resultMap = appSpy.getSortedFiles(rootDir, new TreeMap<>());
        //put empty folder in any place of root-dir or else this will fail(or fix "4" to "3")
        verify(appSpy, times(4)).getSortedFiles(any(File.class), any(TreeMap.class));

        //verify resultMap
        assertThat(resultMap.size()).isEqualTo(3);

        Condition<Collection<File>> containsOnlyFiles =
                new Condition<>(l -> l.stream().allMatch(File::isFile), "only files permitted");
        assertThat(resultMap.values()).is(containsOnlyFiles);
    }

    static File rootFile;

    @BeforeClass
    public static void beforeClass() throws Exception {
        rootFile = new File(AppTest.class.getClassLoader().getResource("root-dir/root-file.txt").toURI());
    }

    @Before
    public void setUp() throws Exception {
        // подготавливаем файлы в тестовых ресурсах
    }


    @Test
    public void testGetFileFromProperties() {
        // нужно реализовывать работу с тестовыми ресурсами
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIfFileExists_withNotExistingFile() throws Exception {
        App app = new App();
        File f = new File("not-existing-file");
        app.checkIfFileExists(f, false);
    }

    @Test
    public void testCheckIfFileExists_withExistingFile() throws Exception {
        try {
            App app = new App();
            app.checkIfFileExists(rootFile, false);
        } catch (Exception ex) {
            fail("fail with unexpected exception");
        }
    }

    @Test
    public void testWriteFile() {
    }

    @Test
    public void testMergeAllFiles() {
    }
}
